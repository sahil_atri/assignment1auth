/**
 * Created by Safer on 11-01-2016.
 */

'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');



var loginRoute = require('./loginRoute');
var createArrayRoute = require('./createArrayRoute');
var postRequestRoute = require('./postRequestRoute');
var uniqueElementsRoute = require('./uniqueElementsRoute');
var putRequestRoute = require('./putRequestRoute');
var getRequestRoute = require('./getRequestRoute');
var deleteRequestRoute = require('./deleteRequestRoute');
var signupRoute = require('./signupRoute');
var logoutRoute = require('./logoutRoute');





module.exports = [
    createArrayRoute.createArray,
    getRequestRoute.getRequest,
    postRequestRoute.postRequest,
    putRequestRoute.putRequest,
    deleteRequestRoute.deleteRequest,
    loginRoute.login,
    logoutRoute.logout,
    signupRoute.signup
];
