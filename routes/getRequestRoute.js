/**
 * Created by Safer on 14-01-2016.
 */



var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');

var getRequest = {
    method: 'GET',
    path:'/v1/search/{key}',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        var returned_index = controller.searchArray(request.params.key);
        if (returned_index !== -1) {
            return reply('element exist at index  '+ returned_index);
        }
        return reply('element doesn\'t exist');
    },
    config: {
        description: 'search',
        notes: 'enter the key',
        tags: ['api'],
        validate: {
            params: {
                key: Joi.number().required().description('key')
            }
        }
    }
};

module.exports = {
    getRequest: getRequest
}