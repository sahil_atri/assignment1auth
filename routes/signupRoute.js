/**
 * Created by Safer on 14-01-2016.
 */

var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');

var signup = {
    method: 'GET',
    path:'/v1/signup/{username}',
    handler: function (request, reply) {
        if (controller.isLoggedIn()) {
            return reply(Error.getError(400, "logout first"));
        }
        var id = controller.signup(request.params.username);
        if (id === null) {
            return reply('user already exists');
        }
        return reply('user added with id ' + id + ' and logged in');
    },
    config: {
        tags: ['api'],
        validate: {
            params: {
                username: Joi.string().required()
            }
        }
    }
};


module.exports = {
    signup: signup
}