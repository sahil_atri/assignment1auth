/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');

var uniqueElements = {
    method: 'POST',
    path:'/v1/uniquearray',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        return reply('unique array elements '+ controller.uniqueArray());
    },
    config: {
        description: 'delete duplicates!',
        tags: ['api']
    }
};


module.exports = {
    uniqueElements: uniqueElements
}