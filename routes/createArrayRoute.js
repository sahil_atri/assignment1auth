/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');

var createArray = {
    method: 'POST',
    path:'/v1/createarray/{data}',
    config: {
        description: 'create array',
        notes: 'enter the array elements',
        tags: ['api'],
        handler: function (request, reply) {
            if (!controller.isLoggedIn()) {
                return reply(Error.getError(401, "login first"));
            }
            return reply('array created ' + controller.createArray(request.params.data));
        },
        validate: {
            params: {
                data: Joi.array().required()
            }
        }
    }
}

module.exports = {
    createArray: createArray
}