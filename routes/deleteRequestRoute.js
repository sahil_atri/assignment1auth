/**
 * Created by Safer on 14-01-2016.
 */
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');

var deleteRequest = {
    method: 'DELETE',
    path:'/v1/delete/{key}',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        return reply(controller.deleteArray(request.params.key));
    },
    config: {
        description: 'delete',
        notes: 'enter the key to be deleted',
        tags: ['api'],
        validate: {
            params: {
                key: Joi.number().required().description('key')
            }
        }
    }
};



module.exports = {
    deleteRequest: deleteRequest
}