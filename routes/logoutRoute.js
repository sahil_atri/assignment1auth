/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');


var logout = {
    method: 'GET',
    path:'/v1/logout',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        controller.logout();
        return reply('logged out successfully');
    },
    config: {
        tags: ['api'],
    }
};



module.exports = {
    logout: logout
}