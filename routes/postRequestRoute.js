/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');


var postRequest = {
    method: 'POST',
    path:'/v1/sort/{sortype}',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        return reply('sorted array  '+ controller.sortArray(request.params.sortype));
    },
    config: {
        description: 'sorting',
        tags: ['api'],
        validate: {
            params: {
                sortype: Joi.string().valid(['quicksort', 'countsort'])
            }
        }
    }
};


module.exports = {
    postRequest: postRequest
}