/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');


var putRequest = {
    method: 'PUT',
    path:'/v1/concatarrays/{array2}',
    handler: function (request, reply) {
        if (!controller.isLoggedIn()) {
            return reply(Error.getError(401, "login first"));
        }
        return reply('new array ' + controller.concatArray(request.params.array2));
    },
    config: {
        description: 'concat array',
        notes: 'enter the array to be concatenated',
        tags: ['api'],
        validate: {
            params: {
                array2: Joi.array().required().description('array2 elements'),
            }
        }
    }
};


module.exports = {
    putRequest: putRequest
}