/**
 * Created by Safer on 14-01-2016.
 */
'use strict'
var Joi = require('joi');
var controller = require('../controller/crudeController');
var Error = require('../Utility/Error');


var login = {
    method: 'POST',
    path: '/v1/login/{username}/{id}',
    config: {
        tags:['api'],
        handler: function(request, reply) {
            if (controller.isLoggedIn()) {
                return reply(Error.getError(400, "logout first"));
            }
            switch (controller.login(request.params.username, request.params.id)) {
                case 0:
                    return reply(Error.getError(401, "username id mismatch"));
                    break;
                case 1:
                    return reply(Error.getError(200, "successfully logged in"));
                    break;
                case -1:
                    return reply(Error.getError(400, "user doesn't exist"));
                    break;
            }
        },
        validate: {
            params: {
                username: Joi.string().required(),
                id: Joi.number().required()
            }
        }
    }
}


module.exports = {
    login: login
}
