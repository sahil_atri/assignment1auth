/**
 * Created by Safer on 12-01-2016.
 */
'use strict'
const Hapi = require('hapi');
var plugins = require('./plugins/plugins');
var serverConfig = require('./Config/serverConfig');
var Pack = require('./package');
var Routes = require('./routes');


const server = new Hapi.Server();

var connectionOptions = {
    host: serverConfig.HOST.LOCAL,
    port: serverConfig.PORT.TEST
};



server.connection(connectionOptions);
server.route(Routes);

server.register(
    plugins,
    function(err) {
        server.start(
            function() {
                console.log('Server running at:', server.info.uri);
            }
        );
    }
);