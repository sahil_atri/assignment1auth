/**
 * Created by Safer on 14-01-2016.
 */


'use strict';

const PORT = {
    TEST: 8000
}

const HOST = {
    LOCAL: 'localhost'
}

module.exports = {
    PORT: PORT,
    HOST: HOST
}