/**
 * Created by Safer on 13-01-2016.
 */

var array = [];
var util = require('../Utility/util');
var id = 2;

var loggers = {
    sahil: 1,//admins
    akshay: 2//admins
};

var currentLogger = null;

var sortArray = function(sortype) {
    util.sortArray(sortype, array);
    return array;
}

var isLoggedIn = function() {
    return currentLogger === null ? false : true;
}
var createArray = function(inp_array) {
    array = inp_array;
    return array;
}

var uniqueArray = function() {
    util.uniqueArray(array);
    return array;
}

var concatArray = function(inp_array) {
    util.concatArray(array, inp_array);
    return array;
}
var searchArray = function(key) {
    return util.searchArray(key, array);
}

var deleteArray = function(key) {
    array = util.deleteArray(key, array);
    return array;
}


/*if username already exists, return null, else add the user*/
var signup = function(username) {
	if (loggers[username] !== undefined) {
		return null;
	}
    
    loggers[username] = ++id;
    currentLogger = id;
    return id;   
}

/*if user doesn't exists return -1, if username-id dont match return 0, if username-id match return 1. */
var login = function(username, id) {
    if(loggers[username] === undefined) {
        return -1;
    }else if(loggers[username] === id) {
        currentLogger = id;
        return 1;
    }else {
        return 0;
    }
}

var logout = function() {
    currentLogger = null;
}


module.exports = {
    searchArray: searchArray,
    createArray: createArray,
    uniqueArray: uniqueArray,
    sortArray: sortArray,
    deleteArray: deleteArray,
    concatArray: concatArray,
    login: login,
    logout: logout,
    signup: signup,
    isLoggedIn: isLoggedIn
};