/**
 * Created by Safer on 14-01-2016.
 */

var Boom = require('boom');



var getError = function(errorCode, message) {
    const error = Boom.badRequest(message);
    error.output.statusCode = errorCode;
    error.reformat();
    return error;
}

module.exports = {
    getError: getError
}

