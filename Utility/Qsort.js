/**
 * Created by Safer on 07-01-2016.
 */

var arr = [6,5,4,3,2,1];

function swap(a, b, arr) {
    var x = arr[a];
    arr[a] = arr[b];
    arr[b] = x;
}
function partition(p, r, arr) {
    var i = p;
    var j = p;
    while (i < r) {
        if (arr[i] <= arr[r]) {
            swap(i, j, arr);
            j ++;
        }
        i ++;
    }
    swap(j, r, arr);
    return j;
}

function qsort(p, r, arr) {
    if (p < r) {
        var pivot = partition(p, r, arr);
        qsort(p, pivot - 1, arr);
        qsort(pivot + 1, r, arr);
    }
}

qsort(0, arr.length - 1, arr);
//alert(arr);


module.exports = [
    qsort
]