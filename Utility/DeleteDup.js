/**
 * Created by Safer on 08-01-2016.
 */
function deleteDup(arr) {
    var hash = {};
    var length = arr.length;
    for (var i = 0 ; i < length ; i ++) {
        if (hash[arr[i]] === undefined) {
            hash[arr[i]] = 1;
        }else {
            hash[arr[i]] = -1;
        }

    }
    arr = [];
    for (var i in hash) {
        if (hash[i] == 1) {
            arr.push(+i);
        }else if(hash[i] == -1) {
            arr.push(+i);
            hash[i] = 0;
        }
    }

    return arr;
}

module.exports = [
    deleteDup
]