/**
 * Created by Safer on 13-01-2016.
 */

var sortArray = require('./Sort')[0];
var uniqueArray = require('./DeleteDup')[0];
var concatArray = require('./Concat')[0];
var searchArray = require('./Search')[0];
var deleteArray = require('./Delete')[0];

module.exports = {
    searchArray: searchArray,
    uniqueArray: uniqueArray,
    sortArray: sortArray,
    deleteArray: deleteArray,
    concatArray: concatArray
};