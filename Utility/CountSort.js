function findFreq(arr) {
    freq = [];
    for (var i = 0 ; i < arr.length ; i ++) {
        if (typeof(freq[arr[i] - 1]) === "undefined") {
            freq[arr[i] - 1] = 1;
        }else {
            freq[arr[i] - 1] ++;
        }
    }
    return freq;
}

function cumulateFreq(freq) {
    for (var i = 1 ; i < freq.length ; i ++) {
        if (typeof(freq[i]) === "undefined") {
            freq[i] = freq[i - 1];
        }else {
            freq[i] += freq[i - 1];
        }
    }
}

function copyArr(arr, temp) {
    for (var i = 0 ; i < arr.length ; i ++) {
        arr[i] = temp[i];
    }
}

function putToPlace(arr, freq) {
    temp = [];
    for (var i = arr.length - 1 ; i >= 0 ; i --) {
        temp[freq[arr[i] - 1] - 1] = arr[i];
        freq[arr[i] - 1] --;
    }
    return temp;
}

function sort(arr) {
    freq = findFreq(arr);
    cumulateFreq(freq);
    temp = putToPlace(arr, freq);
    copyArr(arr, temp);
    //return arr;
}

var arr = sort([5, 5, 2, 1, 1, 3, 5, 7, 7, 2]);

module.exports = [
    sort
]