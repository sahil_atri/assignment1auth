/**
 * Created by Safer on 12-01-2016.
 */

var qsort = require('./Qsort');
var csort = require('./CountSort');

var sort = function(sortype, data) {
    if (sortype === 'quicksort') {
        qsort[0](0, data.length - 1, data);
    }else if (sortype === 'countsort') {
        csort[0](data);
    }
}



module.exports = [
    sort
]